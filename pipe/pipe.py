from bitbucket_pipes_toolkit import Pipe, get_logger
from requests_oauthlib import OAuth2Session
from oauthlib.oauth2 import BackendApplicationClient
from atlassian.bitbucket import Cloud
from datetime import datetime, timezone

logger = get_logger()

schema = {
  'CLIENT_ID': {'type': 'string', 'required': True},
  'CLIENT_SECRET': {'type': 'string', 'required': True},

  'WORKSPACE': {'type': 'string', 'required': True},
  'REPOSLUG': {'type': 'string', 'required': True},
  'DAYS_BEFORE_STALE_ALERT': {'type': 'integer', 'required': False, 'default': 2},
  'STALE_COMMENT': {'type': 'string', 'required': False, 'default': 'This issue is stale because it has been open for 2 days with no activity.'},
  'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}

token_url = "https://bitbucket.org/site/oauth2/access_token"

class StaleAlertPipe(Pipe):
    def run(self):
        super().run()

        logger.info('Getting token for bitbucket cloud...')

        client_id = self.get_variable('CLIENT_ID')
        client_secret = self.get_variable('CLIENT_SECRET')

        client = BackendApplicationClient(client_id=client_id)
        oauth = OAuth2Session(client=client)
        token = oauth.fetch_token(token_url, client_id=client_id, client_secret=client_secret)

        oauth2 = {"client_id": client_id, "token": token}
        bitbucket = Cloud(oauth2=oauth2)

        workspace = self.get_variable('WORKSPACE')
        reposlug = self.get_variable('REPOSLUG')
        days_before_stale_alert = self.get_variable('DAYS_BEFORE_STALE_ALERT')
        stale_comment = self.get_variable('STALE_COMMENT')

        logger.info(f'Looking pending pull requests for {workspace}/{reposlug}...')

        now = datetime.now(timezone.utc)

        w = bitbucket.workspaces.get(workspace)
        p = w.repositories.get(reposlug)
        for pr in p.pullrequests.each(q='state="OPEN"'):
          diff_in_days = (now - pr.updated_on).total_seconds() / 86400
          if diff_in_days > days_before_stale_alert:
            logger.info(f'Notifying pull request #{pr.id}...')
            pr.comment(stale_comment)

        self.success(message="Success!")


if __name__ == '__main__':
    pipe = StaleAlertPipe(pipe_metadata='/pipe.yml', schema=schema)
    pipe.run()
